# AppEditor #

App for Reengineering Android Apps directly on the device. Part of the Bachelor Thesis of Philipp Schreitmüller.
Detailed information [here.](https://bitbucket.org/PhilippSchreitmueller/appeditor/downloads/BachelorThesis.pdf)

