package philipp.schreitmueller.appeditor.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import philipp.schreitmueller.appeditor.R;
/**
 * Handles the application settings
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 *
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
  
}
