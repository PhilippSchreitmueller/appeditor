package philipp.schreitmueller.appeditor.fragments;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import philipp.schreitmueller.appeditor.R;
import philipp.schreitmueller.appeditor.activities.About;
import philipp.schreitmueller.appeditor.activities.Compile;
import philipp.schreitmueller.appeditor.activities.Disclaimer;
import philipp.schreitmueller.appeditor.activities.SettingsActivity;
import philipp.schreitmueller.appeditor.apkediting.EditorMode;
import philipp.schreitmueller.appeditor.apkediting.PrettifyHighlighter;
import philipp.schreitmueller.appeditor.apkediting.SmaliHighlighter;
import philipp.schreitmueller.appeditor.apkediting.EditItem;
import philipp.schreitmueller.appeditor.apkediting.EditHistory;
import philipp.schreitmueller.appeditor.utilities.APKProject;
import philipp.schreitmueller.appeditor.utilities.FileTypes;
import philipp.schreitmueller.appeditor.utilities.Utils;

/**
 * This fragment represents the file editor
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class FileDetailFragment extends Fragment {
	private String filePath;
	private String exFilePath;
	private EditorMode mode;
	private boolean hasChanged;
	private PrettifyHighlighter prettyHighlighter;
	private SmaliHighlighter smaliHighlighter;
	private APKProject project;
	private ContentTextWatcher watcher;
	private EditHistory history;
	private boolean isUndoOrRedo = false;

	/**
	 * 
	 * Async task which loads a text file
	 * 
	 */
	public class LoadFile extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... voids) {
			try {
				return readFileAsString(filePath);
			} catch (IOException e) {
				Toast.makeText(getActivity(), e.getLocalizedMessage(),
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String mResult) {
			if (mResult != null) {
				TextView mView = (TextView) getActivity().findViewById(
						R.id.file_detail);
				ScrollView v = (ScrollView) getActivity().findViewById(
						R.id.smoothScroll);
				v.setSmoothScrollingEnabled(true);
				mView.setText(mResult);
				new SyntaxHighlighter().execute(getCurrentCode());
				registerOnChange();
			}
		}

	}

	/**
	 * Async task to save the text file
	 * 
	 */
	public class SaveFile extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... mStrings) {
			try {
				//mStrings[0] = new String(mStrings[0].getBytes("UTF-8"),"ISO-8859-1");
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
						new FileOutputStream(new File(filePath)));
				outputStreamWriter.write(mStrings[0]);
				outputStreamWriter.close();
			} catch (IOException e) {
				Toast.makeText(getActivity(), e.getLocalizedMessage(),
						Toast.LENGTH_LONG).show();
				Log.e("Exception", "File write failed: " + e.toString());
			}
			return null;
		}

		protected void onPostExecute(Void mResult) {
			getActivity().getActionBar().setSubtitle(
					getActivity().getActionBar().getSubtitle().toString()
							.substring(1));
			Toast.makeText(getActivity(), getResources().getString(R.string.save), Toast.LENGTH_SHORT).show();
			new SyntaxHighlighter().execute(getCurrentCode());
			hasChanged = false;
		}

	}

	/**
	 * 
	 * Async task which handles syntax highlighting
	 * 
	 */
	public class SyntaxHighlighter extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... mStrings) {
			String input = mStrings[0];
			String highlighted = "";
			switch (mode) {
			case SMALI:
				if (!PreferenceManager.getDefaultSharedPreferences(
						getActivity()).getBoolean("pref_ide_smaliHighlight",
						true))
					return input;
				if (smaliHighlighter == null)
					smaliHighlighter = new SmaliHighlighter();
				highlighted = smaliHighlighter.highlightString(input);
				break;
			case XML:
				if (!PreferenceManager.getDefaultSharedPreferences(
						getActivity())
						.getBoolean("pref_ide_xmlHighlight", true)) {
					return input.replace("\n", "\n<br>");
				}
				if (prettyHighlighter == null)
					prettyHighlighter = new PrettifyHighlighter();
				highlighted = prettyHighlighter.highlight(
						Utils.getFileEnding(filePath), input);

				highlighted = highlighted.replace("\n", "\n<br>");
				break;
			default:
				break;

			}
			return highlighted;
		}

		protected void onPostExecute(String mHighlighted) {
			EditText mView = (EditText) getActivity().findViewById(
					R.id.file_detail);
			if (PreferenceManager.getDefaultSharedPreferences(getActivity())
					.getBoolean("pref_ide_smaliHighlight", true)){
				mView.setText(Html.fromHtml(mHighlighted),
						TextView.BufferType.SPANNABLE);
			}
			String subTitle = getActivity().getActionBar().getSubtitle()
					.toString();
			if (subTitle.contains("*")) {
				getActivity().getActionBar().setSubtitle(subTitle.substring(1));
			}
			hasChanged = false;
		}
	}

	public FileDetailFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {
			// Set article based on argument passed in
			filePath = args.getString("file");
			project = (APKProject) args.getSerializable("project");
			getActivity().getActionBar().setSubtitle(R.string.ideLoadingFile);
			setEditorMode();
		}
		history = new EditHistory();
		watcher = new ContentTextWatcher(history);

		View rootView;
		switch (mode) {
		case IMAGE:
			rootView = inflater.inflate(R.layout.fragment_file_img, container,
					false);
			break;
		case XML:
			rootView = inflater.inflate(R.layout.fragment_file_txt, container,
					false);
			break;
		case SMALI:
			rootView = inflater.inflate(R.layout.fragment_file_txt, container,
					false);
			break;
		default:
			rootView = inflater.inflate(R.layout.fragment_file_na, container,
					false);
		}
		setHasOptionsMenu(true);
		return rootView;
	}

	@Override
	public void onStart() {

		super.onStart();
		switch (mode) {
		case IMAGE:
			loadIMAGE();
			break;
		case XML:
			loadXML();
			break;
		case SMALI:
			loadSMALI();
			break;
		case NOTAVAILABLE:
			break;
		}
		getActivity().getActionBar().setSubtitle(
				Utils.getFilenameFromPath(filePath));
	}

	private void loadXML() {
		new LoadFile().execute();
		getActivity().getActionBar().setIcon(R.drawable.ic_launcher);

	}

	private void loadSMALI() {
		new LoadFile().execute();
		getActivity().getActionBar().setIcon(R.drawable.ic_launcher);

		// TODO auto-complete
		String[] suggestions = SmaliHighlighter.getSuggestions();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_expandable_list_item_1, suggestions);
		((AutoCompleteTextView) getActivity().findViewById(R.id.file_detail))
				.setAdapter(adapter);
	}

	private void loadIMAGE() {
		Drawable image = Drawable.createFromPath((filePath));
		if (image.getIntrinsicWidth() > 150)
			image.setBounds(0, 0, 150, 150);
		getActivity().getActionBar().setIcon(image);
		ImageView mImage = (ImageView) getActivity().findViewById(
				R.id.imagePreview);
		mImage.setImageDrawable(image);
		mImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				openInApp(android.content.Intent.ACTION_VIEW);

			}
		});
		((Button) getActivity().findViewById(R.id.viewInApp))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						openInApp(android.content.Intent.ACTION_VIEW);

					}
				});
		((Button) getActivity().findViewById(R.id.shareToApp))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						openInApp(android.content.Intent.ACTION_SEND);

					}
				});

		TextView mImageName = (TextView) getActivity().findViewById(
				R.id.imageName);
		mImageName.setText(Utils.getFilenameFromPath(filePath));
		TextView imageRes = (TextView) getActivity().findViewById(
				R.id.imageResolution);
		imageRes.setText(image.getIntrinsicWidth() + " x "
				+ image.getMinimumHeight());
		((TextView) getActivity().findViewById(R.id.imageFileSize))
				.setText(new File(filePath).length() / 1000f + " KB");
	}

	private void setEditorMode() {

		String mfileEnding = Utils.getFileEnding(filePath);
		if (FileTypes.imgTypes.contains(mfileEnding)) {
			mode = EditorMode.IMAGE;
		} else {
			if (FileTypes.xmlTypes.contains(mfileEnding)) {
				mode = EditorMode.XML;
			} else {
				if (FileTypes.smaliTypes.contains(mfileEnding)) {
					mode = EditorMode.SMALI;
				} else {
					mode = EditorMode.NOTAVAILABLE;
				}
			}
		}
	}

	/**
	 * Starts an intent with the file and the specified action
	 * 
	 * @param action
	 *            Defines which operation is intended
	 */
	private void openInApp(String action) {
		exFilePath = Utils.copyToExternalStorage(getActivity(),
				new File(filePath)).getAbsolutePath();
		if (exFilePath != null) {
			Utils.openInApp(getActivity(), new File(exFilePath), action);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		// Removes the elements from FileListActivity if available
		MenuItem i = menu.findItem(R.id.action_shareProject);
		menu.removeGroup(0);
		//Hack
		if(i!=null)
			menu.add(i.getGroupId(), i.getItemId(), i.getOrder(), i.getTitle());
		inflater.inflate(R.menu.ide, menu);
		if (mode == EditorMode.SMALI || mode == EditorMode.XML
				|| mode == EditorMode.NOTAVAILABLE) {
			menu.findItem(R.id.action_view).setEnabled(false);
		}
		if (mode == EditorMode.NOTAVAILABLE){
			menu.findItem(R.id.action_share).setEnabled(false);
			menu.findItem(R.id.action_save).setEnabled(false);
		}
		if (mode == EditorMode.NOTAVAILABLE || mode == EditorMode.IMAGE) {
			menu.findItem(R.id.action_undo).setEnabled(false);
			menu.findItem(R.id.action_redo).setEnabled(false);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			switch (mode) {
			case IMAGE:
				openInApp(android.content.Intent.ACTION_SEND);
				break;
			case NOTAVAILABLE:
				break;
			case SMALI:
				saveFile();
				break;
			case XML:
				saveFile();
				break;
			default:
				break;
			}
			break;
		case R.id.action_view:
			openInApp(Intent.ACTION_VIEW);
			break;
		case R.id.action_share:
			openInApp(Intent.ACTION_SEND);
			break;
		case R.id.action_undo:
			undo();
			break;
		case R.id.action_redo:
			redo();
			break;
		case R.id.action_smaliHelp:
			startActivity(new Intent(
					Intent.ACTION_VIEW,
					Uri.parse("http://pallergabor.uw.hu/androidblog/dalvik_opcodes.html")));
			break;
		case R.id.action_close:
			getActivity().finish();
			break;
		case R.id.action_about:
			startActivity(new Intent(getActivity(), About.class));
			break;
		case R.id.action_disclaimer:
			startActivity(new Intent(getActivity(), Disclaimer.class));
			break;
		case R.id.action_compile:
			Intent i = new Intent(getActivity(), Compile.class);
			i.putExtra("project", project);
			startActivity(i);
			break;
		case R.id.action_settings:
			Intent settingsActivity = new Intent(getActivity(),
					SettingsActivity.class);
			startActivity(settingsActivity);
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Loads file content from a specified path
	 * 
	 * @param filePath
	 *            Path to the file
	 * @return Content of the specified file
	 * @throws java.io.IOException
	 */
	private String readFileAsString(String filePath) throws java.io.IOException {
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		String line, results = "";

		while ((line = reader.readLine()) != null) {
			results += line;
			results += '\n';
		}
		reader.close();
		return results;
	}

	private void saveFile() {
		if (hasChanged) {
			new SaveFile().execute(getCurrentCode());
		}
	}

	private void registerOnChange() {
		EditText editText = (EditText) getActivity().findViewById(
				R.id.file_detail);
		editText.addTextChangedListener(watcher);
	}

	/**
	 * Gets the current string which is loaded
	 * 
	 * @return String without highlighting which is loadeds
	 */
	private String getCurrentCode() {
		EditText mView = (EditText) getActivity()
				.findViewById(R.id.file_detail);
		return mView.getText().toString();

	}

	/**
	 * Offers status to the embedding activity to find out if content has
	 * changed
	 * 
	 * @return Boolean if is file isn't the original anymore
	 */
	public boolean hasChanged() {
		return hasChanged;
	}

	/**
	 * 
	 * TextWatcher which handles undo/redo actions
	 * 
	 */
	class ContentTextWatcher implements TextWatcher {
		private EditHistory editHistory;
		private CharSequence beforeChange;
		private CharSequence afterChange;

		private ContentTextWatcher(EditHistory history) {
			editHistory = history;
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (!isUndoOrRedo) {
				afterChange = s.subSequence(start, start + count);
				editHistory.add(new EditItem(start, beforeChange, afterChange));
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			if (!isUndoOrRedo) {
				beforeChange = s.subSequence(start, start + count);
			}
		}

		@Override
		public void afterTextChanged(Editable s) {
			hasChanged = true;
			String title = getActivity().getActionBar().getSubtitle()
					.toString();
			if (title.charAt(0) != '*') {
				getActivity().getActionBar().setSubtitle("*" + title);
			}

		}
	}

	/**
	 * Undo the last change on the content
	 */
	private void undo() {
		EditItem edit = history.getPrevious();
		if (edit == null) {
			return;
		}
		Editable text = ((EditText) getActivity()
				.findViewById(R.id.file_detail)).getText();
		int start = edit.getIndex();
		int end = start
				+ (edit.getAfter() != null ? edit.getAfter().length() : 0);
		isUndoOrRedo = true;
		text.replace(start, end, edit.getBefore());
		isUndoOrRedo = false;
		// Remove underlines
		for (Object o : text.getSpans(0, text.length(), UnderlineSpan.class)) {
			text.removeSpan(o);
		}
		Selection.setSelection(text, edit.getBefore() == null ? start
				: (start + edit.getBefore().length()));
	}

	/**
	 * Withdraw a undo operation
	 */
	private void redo() {
		EditItem edit = history.getNext();
		if (edit == null) {
			return;
		}
		Editable text = ((EditText) getActivity()
				.findViewById(R.id.file_detail)).getText();
		int start = edit.getIndex();
		int end = start
				+ (edit.getBefore() != null ? edit.getBefore().length() : 0);
		isUndoOrRedo = true;
		text.replace(start, end, edit.getAfter());
		isUndoOrRedo = false;
		for (Object o : text.getSpans(0, text.length(), UnderlineSpan.class)) {
			text.removeSpan(o);
		}
		Selection.setSelection(text, edit.getAfter() == null ? start
				: (start + edit.getAfter().length()));
	}

}
