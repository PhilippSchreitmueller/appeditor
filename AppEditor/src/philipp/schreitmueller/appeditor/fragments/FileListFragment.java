package philipp.schreitmueller.appeditor.fragments;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import philipp.schreitmueller.appeditor.R;

import philipp.schreitmueller.appeditor.treeview.*;
import philipp.schreitmueller.appeditor.utilities.APKProject;
import philipp.schreitmueller.appeditor.utilities.Utils;

/**
 * Represents a filebrowser which integrates
 * https://code.google.com/p/tree-view-list-android/
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class FileListFragment extends Fragment {

	private APKProject project;
	private final Set<String> selected = new HashSet<String>();
	private TreeViewList treeView;
	private static int LEVEL_NUMBER = 0;
	private TreeStateManager<String> manager = null;
	private SimpleStandardAdapter simpleAdapter;
	OnFileSelectedListener callback;

	public interface OnFileSelectedListener {
		public void onFileSelected(String path);
	}

	public FileListFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Loads the manager from memory in case of screen orientation change
		if (savedInstanceState == null) {
			manager = new InMemoryTreeStateManager<String>();

		} else {
			@SuppressWarnings("unchecked")
			TreeStateManager<String> serializable = (TreeStateManager<String>) savedInstanceState
					.getSerializable("treeManager");
			manager = serializable;
			LEVEL_NUMBER = savedInstanceState.getInt("LEVEL_NUMBER");
			if (manager == null) {
				manager = new InMemoryTreeStateManager<String>();
			}
		}

		View v = inflater.inflate(R.layout.fragment_treeview, container, false);
		treeView = (TreeViewList) v.findViewById(R.id.mainTreeView);
		project = (APKProject) getActivity().getIntent().getSerializableExtra(
				"project");
		if (savedInstanceState == null)
			loadFiles(false);
		else
			loadFiles(true);
		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception.
		try {
			callback = (OnFileSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	/**
	 * Loads the files in the tree
	 * 
	 * @param useCache
	 *            Defines if manager was loaded from memory
	 */
	private void loadFiles(boolean useCache) {
		TreeBuilder<String> mBuilder = new TreeBuilder<String>(manager);
		File mRoot = new File(project.getPath(), "apktool");
		if (!useCache) {
			recursiveFilling(mRoot, mBuilder, 0);
			manager.collapseChildren(null);
		}
		simpleAdapter = new SimpleStandardAdapter(getActivity(), selected,
				manager, LEVEL_NUMBER, callback);
		treeView.setAdapter(simpleAdapter);

	}

	/**
	 * Recursive helper to fill up the file tree and sets LEVEL_NUMBER
	 * 
	 * @param f
	 *            Root file
	 * @param builder
	 *            Treebuilder which manages the build-up
	 * @param depth
	 *            Depth is handed to next recursive call
	 */
	private void recursiveFilling(File f, TreeBuilder<String> builder, int depth) {
		if (depth != 0)
			builder.sequentiallyAddNextNode(f.getAbsolutePath(), depth - 1);
		if (f.isDirectory()) {
			for (File subF : Utils.sortFiles(f.listFiles())) {
				recursiveFilling(subF, builder, depth + 1);
			}
		}
		if (depth > LEVEL_NUMBER)
			LEVEL_NUMBER = depth;

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("treeManager", manager);
		outState.putInt("LEVEL_NUMBER", LEVEL_NUMBER);
		super.onSaveInstanceState(outState);
	}
}
