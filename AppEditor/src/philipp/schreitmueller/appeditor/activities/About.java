package philipp.schreitmueller.appeditor.activities;

import philipp.schreitmueller.appeditor.R;
import android.app.Activity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Activity to display some information about this app.
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class About extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// Make links in text clickable
		((TextView) findViewById(R.id.aboutAPKLink))
				.setMovementMethod(LinkMovementMethod.getInstance());
		((TextView) findViewById(R.id.aboutTreeviewLink))
				.setMovementMethod(LinkMovementMethod.getInstance());
		((TextView) findViewById(R.id.aboutPrettifyLink))
				.setMovementMethod(LinkMovementMethod.getInstance());
		((TextView) findViewById(R.id.aboutZipsignerLink))
				.setMovementMethod(LinkMovementMethod.getInstance());
		((TextView) findViewById(R.id.aboutIconsLink))
				.setMovementMethod(LinkMovementMethod.getInstance());
		((ScrollView) findViewById(R.id.aboutSmooth))
				.setSmoothScrollingEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
