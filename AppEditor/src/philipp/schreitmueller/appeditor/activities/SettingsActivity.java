package philipp.schreitmueller.appeditor.activities;

import philipp.schreitmueller.appeditor.fragments.SettingsFragment;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

/**
 * Standard Activity to display the settings
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Display the fragment as the main content.
		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new SettingsFragment()).commit();
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
