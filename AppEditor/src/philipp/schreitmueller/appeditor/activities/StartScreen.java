package philipp.schreitmueller.appeditor.activities;

import java.io.File;
import java.util.List;

import philipp.schreitmueller.appeditor.R;
import philipp.schreitmueller.appeditor.apkediting.APKAccessMode;
import philipp.schreitmueller.appeditor.apkediting.APKToolFacade;
import philipp.schreitmueller.appeditor.utilities.APKProject;
import philipp.schreitmueller.appeditor.utilities.Utils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.*;
import android.graphics.Color;

/**
 * Entry point of AppEditor. Display existing projects and apps. It is possible
 * to create, to delete projects and to start the decompiling.
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class StartScreen extends Activity {

	private ListView listviewProjects;
	private ListView listViewApps;
	private List<ApplicationInfo> apps;
	private List<APKProject> projects;
	private APKToolFacade apkFacade;

	/**
	 * ArrayAdapter to handle display of installed apps in a list
	 * 
	 */
	public class AppListAdapter extends ArrayAdapter<String> {
		public AppListAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(
					R.layout.activity_start_screen_listitem, parent, false);

			// Check if icons should be disabled to overcome memory problems
			if (PreferenceManager.getDefaultSharedPreferences(
					getApplicationContext()).getBoolean(
					"pref_basics_showIcons", true)) {
				ImageView mView = (ImageView) row.findViewById(R.id.icon);
				mView.setImageDrawable(apps.get(position).loadIcon(
						getPackageManager()));
			}

			TextView mName = (TextView) row.findViewById(R.id.name);
			mName.setText(apps.get(position).loadLabel(getPackageManager()));

			TextView mPackage = (TextView) row.findViewById(R.id.packageName);
			mPackage.setText(apps.get(position).packageName);

			TextView mFileSize = (TextView) row.findViewById(R.id.fileSize);
			Float filesize = getFileSize(position);

			// Critical file sizes should be marked red
			if (filesize > 4)
				mFileSize.setTextColor(Color.RED);
			mFileSize.setText(filesize.toString().subSequence(0, 4) + " MB");
			return row;
		}
	}

	/**
	 * ArrayAdapter to handle display of existing projects
	 * 
	 */
	public class ProjectListAdapter extends ArrayAdapter<String> {
		public ProjectListAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(
					R.layout.activity_start_screen_listitem, parent, false);
			ImageView mView = (ImageView) row.findViewById(R.id.icon);
			mView.setImageResource(R.drawable.ic_launcher);

			TextView mName = (TextView) row.findViewById(R.id.name);
			mName.setText(projects.get(position).getProjectName());

			TextView mPackage = (TextView) row.findViewById(R.id.packageName);
			mPackage.setText(projects.get(position).getPackageName());

			return row;
		}

	}

	/**
	 * Async Task which retrieves installed apps/projects and creates the needed
	 * ArrayAdapters
	 */
	public class LoadAndSort extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			apps = Utils.getAppList(getApplicationContext());
			projects = Utils.getProjectList(getApplicationContext());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			AppListAdapter mAppAdapter = new AppListAdapter(
					getApplicationContext(),
					R.layout.activity_start_screen_listitem,
					new String[apps.size()]);
			listViewApps.setAdapter(mAppAdapter);

			// Open NewProjectDialog by clicking on listitem
			listViewApps.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					if (arg3 < apps.size()) {
						showNewProjectDialog(apps.get(arg2).packageName,
								getFileSize(arg2) > 4.0f ? true : false);
					}
				}
			});
			ProjectListAdapter mProjectAdapter = new ProjectListAdapter(
					getApplicationContext(),
					R.layout.activity_start_screen_listitem,
					new String[projects.size()]);
			listviewProjects.setAdapter(mProjectAdapter);
			if(projects.size()==0)
				((TextView)findViewById(R.id.startScreenProjects)).setText(R.string.startTxt1NA);
			else
				((TextView)findViewById(R.id.startScreenProjects)).setText(R.string.startTxt1);
			// Open FileBrowser by clicking on project
			listviewProjects.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					openProject(projects.get(arg2));

				}

			});
			
			// Open DeleteProjectDialog for a long-click on a project
			listviewProjects
					.setOnItemLongClickListener(new OnItemLongClickListener() {

						@Override
						public boolean onItemLongClick(AdapterView<?> arg0,
								View arg1, int arg2, long arg3) {
							showDeleteProjectDialog(projects.get(arg2));
							return true;
						}

					});

			getActionBar().setSubtitle(R.string.startSubTitleStart);
			setProgressBarIndeterminateVisibility(false);

		}
	}

	/**
	 * Async Task to decompile a selected APK and start the filebrowser activity
	 */
	public class DecompileAsync extends AsyncTask<APKProject, Void, APKProject> {

		@Override
		protected APKProject doInBackground(APKProject... params) {
			apkFacade.decompile(APKAccessMode.OFFLINE, params[0]);
			return params[0];
		}

		@Override
		protected void onPostExecute(APKProject project) {
			setProgressBarIndeterminateVisibility(false);
			getActionBar().setSubtitle(R.string.startSubtitleDecompileFinished);
			openProject(project);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		Utils.checkExecutables(getApplicationContext());
		setContentView(R.layout.activity_start_screen);
		apkFacade = new APKToolFacade(getApplicationContext());
		setProgressBarIndeterminateVisibility(true);

		// get Reference to the listviews
		listViewApps = (ListView) findViewById(R.id.listviewApps);
		listviewProjects = (ListView) findViewById(R.id.listviewProjects);

		// start loading async
		new LoadAndSort().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.start_screen, menu);
		getActionBar().setSubtitle(R.string.startSubTitleLoad);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_refresh:
			refresh();
			break;
		case R.id.action_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			break;
		case R.id.action_about:
			startActivity(new Intent(this, About.class));
			break;
		case R.id.action_disclaimer:
			startActivity(new Intent(this, Disclaimer.class));
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Creates new project by prompting the user for a project name.
	 * 
	 * @param packageName
	 *            The package name of the app which should be decompiled
	 * @param bigFile
	 *            Determines if the file is bigger than 4 MB
	 */
	private void showNewProjectDialog(String packageName, boolean bigFile) {
		final String mPackageName = packageName;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// add warning if the file is big
		if (bigFile) {
			builder.setTitle(getResources().getString(
					R.string.startCreateProjectName)
					+ "\n"
					+ getResources().getString(R.string.startScreenDeFail));
		} else {
			builder.setTitle(R.string.startCreateProjectName);
		}
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton(R.string.startScreenOk,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// continues the create process of a new project
						createProject(mPackageName, input.getText().toString()
								.trim());
					}
				});
		builder.setNegativeButton(R.string.startScreenCancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();
	}

	/**
	 * Dialog which ensures if a project should be deleted
	 * 
	 * @param project
	 *            APKProject which should be deleted
	 */
	private void showDeleteProjectDialog(APKProject project) {
		final APKProject mFinalP = project;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.startDeleteProject);
		builder.setPositiveButton(R.string.startScreenOk,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// delete Projects
						Utils.deleteProject(getApplicationContext(), mFinalP);
						refresh();
					}
				});
		builder.setNegativeButton(R.string.startScreenCancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();

					}
				});
		builder.show();
	}

	/**
	 * Project and apps lists are refreshed
	 */
	private void refresh() {
		getActionBar().setSubtitle(R.string.startSubTitleLoad);
		setProgressBarIndeterminateVisibility(true);
		new LoadAndSort().execute();

	}

	/**
	 * Creates an APKProject Object for the new projects and starts decompiling
	 * 
	 * @param packageName
	 *            PackageName of the app which should be decompiled
	 * @param projectName
	 *            Project name which the user typed in before
	 */
	private void createProject(String packageName, String projectName) {
		setProgressBarIndeterminateVisibility(true);
		getActionBar().setSubtitle(R.string.startSubTitleDecompile);
		APKProject mProject = new APKProject();
		mProject.setPackageName(packageName);
		mProject.setProjectName(projectName);
		mProject.setPath(getFilesDir().getAbsolutePath() + "/" + packageName
				+ "/" + projectName);
		new DecompileAsync().execute(mProject);
	}

	/**
	 * Starts an intent to show the filebrowser for the selected project
	 * 
	 * @param project
	 *            Project which should be opened in the file browser
	 */
	private void openProject(APKProject project) {
		Intent mDetailView = new Intent(this, FileListActivity.class);
		mDetailView.putExtra("project", project);
		startActivity(mDetailView);

	}

	/**
	 * Calculates the file size of an App
	 * 
	 * @param position
	 *            The position of the app in the app list
	 * @return Returns the file size in MB
	 */
	private float getFileSize(int position) {
		if (apps != null && position < apps.size()) {
			Float filesize = new File(apps.get(position).sourceDir).length() / 1000000f;
			return filesize;
		}
		return 0.0f;
	}
}
