package philipp.schreitmueller.appeditor.activities;

import philipp.schreitmueller.appeditor.R;
import philipp.schreitmueller.appeditor.fragments.FileDetailFragment;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

/**
 * FragmentActivity to display file details. It is ONLY used used on small
 * displays.
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 */
public class FileDetailActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_detail);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		/*
		 * FileDetailFragment must be generated and added to the activity
		 */
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString("file", getIntent().getStringExtra("file"));
			arguments.putSerializable("project", getIntent().getSerializableExtra("project"));
			FileDetailFragment fragment = new FileDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.file_detail_container, fragment).commit();

		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			checkSave();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {

		checkSave();
	}

	/**
	 * Checks if the files was changed. If yes, a dialog is started
	 */
	private void checkSave() {
		final FileDetailFragment f = (FileDetailFragment) getSupportFragmentManager()
				.findFragmentById(R.id.file_detail_container);
		if (f.hasChanged()) {

			// Set up the dialog
			AlertDialog.Builder builder = new AlertDialog.Builder(
					f.getActivity());
			builder.setTitle(R.string.ideCloseWithoutSave);
			builder.setPositiveButton(R.string.ideCloseWithoutSaveAbort,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			builder.setNegativeButton(R.string.ideCloseWithoutSaveYes,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();

						}
					});

			builder.show();
		} else {
			finish();
		}

	}
}
