package philipp.schreitmueller.appeditor.activities;

import java.io.File;

import kellinwood.security.zipsigner.ZipSigner;
import philipp.schreitmueller.appeditor.R;
import philipp.schreitmueller.appeditor.apkediting.APKAccessMode;
import philipp.schreitmueller.appeditor.apkediting.APKToolFacade;
import philipp.schreitmueller.appeditor.utilities.APKProject;
import philipp.schreitmueller.appeditor.utilities.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * With this Activity you can recompile decompiled sources.
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */

public class Compile extends Activity {

	private APKProject apkkProject;
	private APKToolFacade apkFacade;
	private String intPath;
	private String exPath;
	private String exPathSigned;

	/**
	 * Background task to perform compiling and signing with the testkey
	 * 
	 */
	public class CompileSignAsync extends AsyncTask<APKProject, Void, String> {

		@Override
		protected String doInBackground(APKProject... params) {
			String failText = "1";
			if (!params[0].getApkName().endsWith("-unsigned.apk"))
				
			params[0].setApkName(params[0].getApkName().replace(".apk",
					"-unsigned.apk"));
			try {
				apkFacade.compile(APKAccessMode.OFFLINE, params[0]);
			} catch (Throwable e) {
				failText = e.getLocalizedMessage();

			}

			try {
				/*
				 * Setting the internal apk, external apk and external-signed
				 * apk paths
				 */
				intPath = apkkProject.getPath() + "/apk/"
						+ apkkProject.getApkName();
				exPath = Utils.copyToExternalStorage(getApplicationContext(),
						new File(intPath)).getAbsolutePath();
				exPathSigned = exPath.replace("-unsigned.apk", ".apk");
				// Sign with the built-in default test key/certificate.
				ZipSigner zipSigner = new ZipSigner();
				zipSigner.setKeymode(ZipSigner.KEY_TESTKEY);
				zipSigner.signZip(exPath, exPathSigned);
			} catch (Exception t) {
				// Inform user about problems
				Toast.makeText(getApplicationContext(), t.getMessage(),
						Toast.LENGTH_LONG).show();
			}
			return failText;
		}

		@Override
		protected void onPostExecute(String result) {
			setProgressBarIndeterminateVisibility(false);
			if (result.equals("1")) {
				getActionBar().setSubtitle(R.string.compileFinished);
				findViewById(R.id.compileSend).setEnabled(true);
				findViewById(R.id.compileOpen).setEnabled(true);
			} else {
				getActionBar().setSubtitle(R.string.compileFailShort);
				showFailDialog(result);
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_compile);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setSubtitle(R.string.compileReady);
		apkkProject = (APKProject) getIntent().getSerializableExtra("project");

		// Handle wrong intent
		if (apkkProject == null) {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.projectNotFound),
					Toast.LENGTH_LONG).show();
			finish();
		}
		apkFacade = new APKToolFacade(getApplicationContext());

		EditText apkName = (EditText) findViewById(R.id.compileName);
		apkName.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String name = s.toString();
				Button start = (Button) findViewById(R.id.compileStart);

				/*
				 * By editing the Name, the activity is reset
				 */
				findViewById(R.id.compileSend).setEnabled(false);
				findViewById(R.id.compileOpen).setEnabled(false);

				if (name.matches("[A-Za-z0-9]+\\.apk")) {
					apkkProject.setApkName(name);
					getActionBar().setSubtitle(R.string.compileReady);
					start.setEnabled(true);

				} else {
					getActionBar().setSubtitle(R.string.compileMalFormed);
					start.setEnabled(false);
				}

			}
		});
		apkName.setText(apkkProject.getProjectName() + ".apk");

		((Button) findViewById(R.id.compileStart))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						getActionBar().setSubtitle(R.string.compileInProgress);
						setProgressBarIndeterminateVisibility(true);
						new CompileSignAsync().execute(apkkProject);

					}
				});
		((Button) findViewById(R.id.compileSend))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						Utils.openInApp(getApplicationContext(), new File(
								exPathSigned),
								android.content.Intent.ACTION_SEND);

					}
				});
		((Button) findViewById(R.id.compileOpen))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Utils.openInApp(getApplicationContext(), new File(
								exPathSigned),
								android.content.Intent.ACTION_VIEW);

					}
				});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showFailDialog(String problem) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.compileFail);

		final TextView input = new TextView(getApplicationContext());
		input.setText(problem);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton(R.string.startScreenOk,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
		builder.show();

	}
}