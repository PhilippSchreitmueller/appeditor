package philipp.schreitmueller.appeditor.activities;

import java.io.File;
import java.io.IOException;
import philipp.schreitmueller.appeditor.R;
import philipp.schreitmueller.appeditor.fragments.FileDetailFragment;
import philipp.schreitmueller.appeditor.fragments.FileListFragment;
import philipp.schreitmueller.appeditor.utilities.APKProject;
import philipp.schreitmueller.appeditor.utilities.Utils;
import android.support.v4.app.FragmentTransaction;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

/**
 * FragmentActivity which displays a file-tree for the selected project. On
 * small display only the tree is shown. On bigger screens tree and file details
 * are shown
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class FileListActivity extends FragmentActivity implements
		FileListFragment.OnFileSelectedListener {

	private APKProject apkProject;
	private boolean twoPane;
	private boolean fileOpened = false;

	/**
	 * Async task which packs the project into a zip file
	 * 
	 */
	public class CompressProject extends AsyncTask<String, Void, String[]> {

		@Override
		protected String[] doInBackground(String... mStrings) {
			try {
				Utils.zipDirectory(new File(apkProject.getPath() + "/apktool"),
						new File(mStrings[0]));
			} catch (IOException e) {
				e.printStackTrace();
			}

			return mStrings;
		}

		@Override
		protected void onPostExecute(String[] mResult) {

			// Start a send intent with the zip file attached
			MimeTypeMap mime = MimeTypeMap.getSingleton();
			Intent intent = new Intent(Intent.ACTION_SEND);
			File file = new File(mResult[0]);
			String ext = Utils.getFileEnding(file.getAbsolutePath());
			String type = mime.getMimeTypeFromExtension(ext);
			Uri url = Uri.fromFile(file);
			intent.setType(type);
			intent.putExtra(Intent.EXTRA_STREAM, url);
			startActivity(intent);
			getActionBar().setSubtitle(mResult[1]);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_list);
		apkProject = (APKProject) getIntent().getSerializableExtra("project");
		if (apkProject == null) {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.projectNotFound),
					Toast.LENGTH_LONG).show();
			finish();
		}
		getActionBar().setTitle(
				getResources().getString(R.string.flTitleOverview) + " - "
						+ apkProject.getProjectName());
		getActionBar().setSubtitle(R.string.flChooseFile);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(R.drawable.clipboard);

		// Check if in twoPane mode
		if (findViewById(R.id.file_detail_container) != null) {

			twoPane = true;
		}
	}

	@Override
	public void onFileSelected(final String path) {
		fileOpened = true;

		// different handling for twopane or singlepane layouts
		if (twoPane) {
			final FileDetailFragment f = (FileDetailFragment) getSupportFragmentManager()
					.findFragmentById(R.id.file_detail_container);

			// show dialog if user is about to close unsaved file
			if (f != null && f.hasChanged()) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						f.getActivity());
				builder.setTitle(R.string.ideCloseWithoutSave);
				builder.setPositiveButton(R.string.ideCloseWithoutSaveAbort,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						});
				builder.setNegativeButton(R.string.ideCloseWithoutSaveYes,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								getActionBar().setSubtitle(
										R.string.ideLoadingFile);
								FileDetailFragment detailFragment = new FileDetailFragment();
								Bundle args = new Bundle();
								args.putString("file", path);
								args.putSerializable("project", apkProject);
								detailFragment.setArguments(args);
								FragmentTransaction trans = getSupportFragmentManager()
										.beginTransaction();
								trans.replace(R.id.file_detail_container,
										detailFragment);
								trans.addToBackStack(null);
								trans.commit();

							}
						});
				builder.show();

			}
			// first file is loaded or nothing has changed in current file
			if (f == null || !f.hasChanged()) {
				getActionBar().setSubtitle(R.string.ideLoadingFile);
				FileDetailFragment detailFragment = new FileDetailFragment();
				Bundle args = new Bundle();
				args.putString("file", path);
				args.putSerializable("project", apkProject);
				detailFragment.setArguments(args);
				FragmentTransaction trans = getSupportFragmentManager()
						.beginTransaction();
				trans.replace(R.id.file_detail_container, detailFragment);
				trans.addToBackStack(null);
				trans.commit();
			}

		} else {

			Intent detailIntent = new Intent(this, FileDetailActivity.class);
			detailIntent.putExtra("file", path);
			detailIntent.putExtra("project", apkProject);
			startActivity(detailIntent);
		}

	}

	/**
	 * 
	 * @return Returns current APKProject
	 */
	public APKProject getProject() {
		return apkProject;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// menu is added but removed when fragment has file opened
		getMenuInflater().inflate(R.menu.filebrowser, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Handle context menu clicks if in single-pane mode or two-pane with no
		// file opened
		if (!twoPane || !fileOpened) {
			switch (item.getItemId()) {
			case R.id.action_about:
				startActivity(new Intent(this, About.class));
				break;
			case R.id.action_disclaimer:
				startActivity(new Intent(this, Disclaimer.class));
				break;
			case R.id.action_compile:
				Intent i = new Intent(this, Compile.class);
				i.putExtra("project", apkProject);
				startActivity(i);
				break;
			case R.id.action_settings:
				Intent settingsActivity = new Intent(getApplicationContext(),
						SettingsActivity.class);
				startActivity(settingsActivity);
				break;
			case R.id.action_close:
				finish();
				break;
			default:

			}
		}
		// always handle share project click
		if (item.getItemId() == R.id.action_shareProject)
			shareProject();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Start the Async Task which zips the projects and starts an intent
	 */
	private void shareProject() {
		String oldSubtitle = getActionBar().getSubtitle().toString();
		getActionBar().setSubtitle(R.string.flZippingProject);
		File exFile = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile()
				+ "/AppEditor/"
				+ apkProject.getPackageName()
				+ "/"
				+ apkProject.getProjectName() + "/zip");
		exFile.mkdirs();
		new CompressProject().execute(exFile.getAbsolutePath() + "/"
				+ apkProject.getProjectName() + ".zip", oldSubtitle);

	}
}
