package philipp.schreitmueller.appeditor.treeview;

import java.util.Set;

import philipp.schreitmueller.appeditor.R;
import philipp.schreitmueller.appeditor.fragments.FileListFragment.OnFileSelectedListener;
import philipp.schreitmueller.appeditor.utilities.FileTypes;
import philipp.schreitmueller.appeditor.utilities.Utils;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Handles file tree item representation
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class SimpleStandardAdapter extends AbstractTreeViewAdapter<String> {

	private OnFileSelectedListener callback;
	private String selectedFile = "";

	public SimpleStandardAdapter(final Activity treeViewListDemo,
			final Set<String> selected,
			final TreeStateManager<String> treeStateManager,
			final int numberOfLevels, final OnFileSelectedListener mCallback) {
		super(treeViewListDemo, treeStateManager, numberOfLevels);
		callback = mCallback;
	}

	@Override
	public View getNewChildView(final TreeNodeInfo<String> treeNodeInfo) {
		final LinearLayout viewLayout = (LinearLayout) getActivity()
				.getLayoutInflater().inflate(R.layout.fragment_treeview_item,
						null);
		return updateView(viewLayout, treeNodeInfo);
	}

	@Override
	public LinearLayout updateView(final View view,
			final TreeNodeInfo<String> treeNodeInfo) {
		final LinearLayout viewLayout = (LinearLayout) view;
		final TextView descriptionView = (TextView) viewLayout
				.findViewById(R.id.demo_list_item_description);
		String name = treeNodeInfo.getId();
		descriptionView.setText(Utils.getFilenameFromPath(name));
		
		//Make expanded folder a bit bigger
		if (treeNodeInfo.isExpanded()) {
			descriptionView.setTextSize(16f);
		} else {
			descriptionView.setTextSize(14f);
		}
		return viewLayout;
	}

	@Override
	public void handleItemClick(final View view, final Object id) {
		final String longId = (String) id;
		final TreeNodeInfo<String> info = getManager().getNodeInfo(longId);
		if (info.isWithChildren()) {
			super.handleItemClick(view, id);
		} else {
			callback.onFileSelected(longId);
			selectedFile = longId;
			refresh();

		}
	}

	@Override
	public Drawable getBackgroundDrawable(
			final TreeNodeInfo<String> treeNodeInfo) {
		if (selectedFile.equals(treeNodeInfo.getId()))
			return new ColorDrawable(Color.WHITE);
		else if (treeNodeInfo.isWithChildren())
			return null;

		return null;

	}

	@Override
	protected Drawable getDrawable(final TreeNodeInfo<String> nodeInfo) {
		if (!nodeInfo.isWithChildren()) {
			String fileEnding = Utils.getFileEnding(nodeInfo.getId());
			if (FileTypes.smaliTypes.contains(fileEnding))
				return getActivity().getResources().getDrawable(
						R.drawable.script);
			else if (FileTypes.imgTypes.contains(fileEnding))
				return getActivity().getResources().getDrawable(
						R.drawable.photo);
			return getActivity().getResources().getDrawable(R.drawable.file);
		} else {
			return super.getDrawable(nodeInfo);
		}

	}

	@Override
	public long getItemId(final int position) {
		return (long) position;
	}

}