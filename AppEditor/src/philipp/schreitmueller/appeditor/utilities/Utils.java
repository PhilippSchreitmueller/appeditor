package philipp.schreitmueller.appeditor.utilities;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import philipp.schreitmueller.appeditor.R;

import android.content.Context;
import android.content.Intent;
import android.content.pm.*;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

/**
 * Defines various helper methods which are needed in different files
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmülelr
 * 
 */
public class Utils {

	/**
	 * Gets a list of all installed applications
	 * 
	 * @param c
	 *            Context of the applicaiton
	 * @return ApplicationInfo List with all installed apps
	 */
	public static List<ApplicationInfo> getAppList(Context c) {
		PackageManager pm = c.getPackageManager();
		List<ApplicationInfo> appinfo_list = pm.getInstalledApplications(0);
		return sortAppList(appinfo_list, c);
	}

	/**
	 * Gets a list with all available projects
	 * 
	 * @param c
	 *            Context of the application
	 * @return List of APKProjects
	 */
	public static List<APKProject> getProjectList(Context c) {
		File baseDir = c.getFilesDir();
		List<APKProject> projects = new ArrayList<APKProject>();
		for (File mPackage : baseDir.listFiles()) {
			if (mPackage.isDirectory()
					&& !mPackage.getAbsolutePath().endsWith("exec")) { // ignore
																		// exec
																		// folder
				for (File mProject : mPackage.listFiles()) {
					APKProject project = new APKProject();
					project.setPackageName(mPackage.getName());
					project.setProjectName(mProject.getName());
					project.setPath(mProject.getAbsolutePath());
					projects.add(project);
				}
			}
		}
		return projects;

	}

	/**
	 * Sorts a list of Applications by label name
	 * 
	 * @param appList
	 *            List which should be sorted
	 * @param c
	 *            Context of the application
	 * @return Sorted list of apps
	 */
	public static List<ApplicationInfo> sortAppList(
			List<ApplicationInfo> appList, final Context c) {
		Collections.sort(appList, new Comparator<ApplicationInfo>() {

			@Override
			public int compare(ApplicationInfo lhs, ApplicationInfo rhs) {
				int ret = 0;
				try {
					ret = (lhs.loadLabel(c.getPackageManager()) + "")
							.compareToIgnoreCase(rhs.loadLabel(c
									.getPackageManager()) + "");
				} catch (Exception e) {

				}

				return ret;
			}

		});
		return appList;
	}

	/**
	 * Deletes a APKProject
	 * 
	 * @param c
	 *            Context of the applicaiton
	 * @param project
	 *            Project which should be deleted
	 */
	public static void deleteProject(Context c, APKProject project) {
		File base = new File(c.getFilesDir(), project.getPackageName());
		File projectDir = new File(base, project.getProjectName());
		deleteRecursive(projectDir);
	}

	/**
	 * Helper to delete folders recursively
	 * 
	 * @param f
	 *            Root folder
	 */
	private static void deleteRecursive(File f) {
		if (f.isDirectory()) {
			File[] children = f.listFiles();
			for (int i = 0; i < children.length; i++) {

				if (children[i].isDirectory()) {
					deleteRecursive(children[i]);
				} else {
					children[i].delete();
				}
			}
			f.delete();
		} else {
			f.delete();
		}
	}

	/**
	 * Checks if the execs 1.apk and aapt have been transferred yet. If not it
	 * copies them to the filesystem
	 * 
	 * @param c
	 *            Context of the app
	 * @return True in case of success
	 */
	public static boolean checkExecutables(Context c) {
		InputStream ins;
		byte[] buffer;
		FileOutputStream fos;
		File framework = new File(c.getFilesDir().getAbsolutePath()
				+ "/exec/1.apk");
		File aapt = new File(c.getFilesDir().getAbsolutePath() + "/exec/aapt");

		if (!framework.exists()) {
			File mFolder = new File(c.getFilesDir() + "/exec");
			mFolder.mkdir();
			try {
				ins = c.getResources().openRawResource(R.raw.framework);
				buffer = new byte[ins.available()];
				ins.read(buffer);
				ins.close();
				fos = new FileOutputStream(framework);
				fos.write(buffer);
				fos.close();

			} catch (IOException e) {
				Toast.makeText(c, e.getLocalizedMessage(), Toast.LENGTH_LONG)
						.show();
				return false;
			}
		}
		if (!aapt.exists()) {
			try {
				ins = c.getResources().openRawResource(R.raw.aapt);
				buffer = new byte[ins.available()];
				ins.read(buffer);
				ins.close();
				fos = new FileOutputStream(aapt);
				fos.write(buffer);
				fos.close();

			} catch (IOException e) {
				Toast.makeText(c, e.getLocalizedMessage(), Toast.LENGTH_LONG)
						.show();
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the file name from a path
	 * 
	 * @param path
	 *            Path to the file
	 * @return File name
	 */
	public static String getFilenameFromPath(String path) {
		int h = path.lastIndexOf('/') + 1;
		return (path.substring(h));
	}

	/**
	 * Copies a file from src to dst
	 * 
	 * @param src
	 *            Source file
	 * @param dst
	 *            Destination file
	 * @throws IOException
	 */
	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	/**
	 * Zips a directory into a file
	 * 
	 * @param directory
	 *            Directory which should be compressed
	 * @param zip
	 *            Destination zip file
	 * @throws IOException
	 */
	public static final void zipDirectory(File directory, File zip)
			throws IOException {
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zip));
		zip(directory, directory, zos);
		zos.close();
	}

	private static final void zip(File directory, File base, ZipOutputStream zos)
			throws IOException {
		File[] files = directory.listFiles();
		byte[] buffer = new byte[8192];
		int read = 0;
		for (int i = 0, n = files.length; i < n; i++) {
			if (files[i].isDirectory()) {
				zip(files[i], base, zos);
			} else {
				FileInputStream in = new FileInputStream(files[i]);
				ZipEntry entry = new ZipEntry(files[i].getPath().substring(
						base.getPath().length() + 1));
				zos.putNextEntry(entry);
				while (-1 != (read = in.read(buffer))) {
					zos.write(buffer, 0, read);
				}
				in.close();
			}
		}
	}

	/**
	 * Unzips a archive to a specified destination
	 * 
	 * @param zip
	 *            Zip file
	 * @param extractTo
	 *            Destination folder
	 * @throws IOException
	 */
	public static final void unzip(File zip, File extractTo) throws IOException {
		ZipFile archive = new ZipFile(zip);
		@SuppressWarnings("rawtypes")
		Enumeration e = archive.entries();
		while (e.hasMoreElements()) {
			ZipEntry entry = (ZipEntry) e.nextElement();
			File file = new File(extractTo, entry.getName());
			if (entry.isDirectory() && !file.exists()) {
				file.mkdirs();
			} else {
				if (!file.getParentFile().exists()) {
					file.getParentFile().mkdirs();
				}

				InputStream in = archive.getInputStream(entry);
				BufferedOutputStream out = new BufferedOutputStream(
						new FileOutputStream(file));

				byte[] buffer = new byte[8192];
				int read;

				while (-1 != (read = in.read(buffer))) {
					out.write(buffer, 0, read);
				}

				in.close();
				out.close();
			}
		}
	}

	/**
	 * Sorts an array of files alphabetically
	 * 
	 * @param files
	 *            Array of files to be sorted
	 * @return Sorted file array
	 */
	public static File[] sortFiles(File[] files) {
		Arrays.sort(files, new Comparator<File>() {

			@Override
			public int compare(File lhs, File rhs) {
				if (lhs.isDirectory() && !rhs.isDirectory()
						|| !lhs.isDirectory() && rhs.isDirectory()) {
					if (lhs.isDirectory())
						return -1;
					else
						return 1;
				}
				return lhs.getName().compareTo(rhs.getName());

			}
		});
		return files;
	}

	/**
	 * Gets the file ending from a file path
	 * 
	 * @param file
	 *            Filepath
	 * @return The file ending
	 */
	public static String getFileEnding(String file) {
		int Index = file.lastIndexOf(".");
		String fileEnding = file.substring(Index + 1)
				.toLowerCase(Locale.GERMAN);
		return fileEnding;

	}

	/**
	 * Copies a file from a project to a public available Location
	 * 
	 * @param c
	 *            Context to retrieve Base Directories
	 * @param file
	 *            Internal File to be copied to external storage
	 * @return External File
	 */
	public static File copyToExternalStorage(Context c, File file) {
		String intPath = file.getAbsolutePath();
		String subPart = intPath.substring(intPath.lastIndexOf("files") + 5,
				intPath.lastIndexOf("/"));
		File exFile = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile()
				+ "/AppEditor" + subPart);
		exFile.mkdirs();
		String exPath = exFile + "/" + Utils.getFilenameFromPath(intPath);
		//smali hack
		if(exPath.endsWith(".smali"))
			exPath = exPath.replace(".smali", ".txt");
		
		//if (!new File(exPath).exists()) {
			try {
				Utils.copy(new File(intPath), new File(exPath));
			} catch (Exception e) {
				Log.d("Copy error", e.getLocalizedMessage());
		//	}
		}
		File kEx = new File(exPath);
		kEx.setExecutable(true);
		kEx.setReadable(true);
		kEx.setWritable(true);
		return kEx;
	}

	/**
	 * Starts an intent for a file
	 * 
	 * @param c
	 *            Context of the app
	 * @param file
	 *            File with should be intended
	 * @param action
	 *            Intended Action
	 */
	public static void openInApp(Context c, File file, String action) {
		Intent intent = new Intent();
		intent.setAction(action);

		MimeTypeMap mime = MimeTypeMap.getSingleton();
		String ext = file.getName()
				.substring(file.getName().indexOf(".") + 1);

		String type = mime.getMimeTypeFromExtension(ext);
		Uri url = Uri.fromFile(file);
		if (action == Intent.ACTION_SEND) {
			intent.setType(type);
			intent.putExtra(Intent.EXTRA_STREAM, url);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} else {

			intent.setDataAndType(url, type);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		}
		c.startActivity(intent);

	}
}
