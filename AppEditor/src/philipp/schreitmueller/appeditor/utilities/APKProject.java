package philipp.schreitmueller.appeditor.utilities;

import java.io.Serializable;

/**
 * Datatype for a Decompiling project
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class APKProject implements Serializable {

	private static final long serialVersionUID = 1L;
	private String packageName;
	private String projectName;
	private String path;
	private String apkName;

	public String getApkName() {
		return apkName;
	}

	public void setApkName(String apkName) {
		this.apkName = apkName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
