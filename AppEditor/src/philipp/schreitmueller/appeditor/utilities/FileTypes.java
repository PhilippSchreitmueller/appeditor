package philipp.schreitmueller.appeditor.utilities;

import java.util.Arrays;
import java.util.List;

/**
 * Handles lists of file endings which correspond to the available editor modes
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class FileTypes {

	public static List<String> xmlTypes = Arrays.asList("xml", "yml");
	public static List<String> imgTypes = Arrays.asList("png", "bmp", "jpg");
	public static List<String> smaliTypes = Arrays.asList("smali");
}
