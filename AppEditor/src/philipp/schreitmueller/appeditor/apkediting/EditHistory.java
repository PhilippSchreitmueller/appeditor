package philipp.schreitmueller.appeditor.apkediting;

import java.util.Vector;

/**
 * Manages a vector of EditItems
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller adapted from
 *         https://code.google.com/r/credentiality-android-scripting/
 * 
 */
public class EditHistory {
	int position = 0;
	public final Vector<EditItem> history = new Vector<EditItem>();

	/**
	 * Adds a new edit operation to the history at the current position. If
	 * executed after a call to getPrevious() removes all the future history
	 * (elements with positions >= current history position).
	 * 
	 */
	public void add(EditItem item) {
		history.setSize(position);
		history.add(item);
		position++;
	}

	/**
	 * Traverses the history backward by one position, returns and item at that
	 * position.
	 */
	public EditItem getPrevious() {
		if (position == 0) {
			return null;
		}
		position--;
		return history.get(position);
	}

	/**
	 * Traverses the history forward by one position, returns and item at that
	 * position.
	 */
	public EditItem getNext() {
		if (position == history.size()) {
			return null;
		}
		EditItem item = history.get(position);
		position++;
		return item;
	}
}
