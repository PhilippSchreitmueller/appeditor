package philipp.schreitmueller.appeditor.apkediting;

import philipp.schreitmueller.appeditor.utilities.APKProject;

/**
 * apktool is executed on application server. Can be implemented in the future
 * if necessary
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class APKToolOnline implements IAPKTool {

	@Override
	public void decompile(APKProject mProject) {
	}

	@Override
	public void compile(APKProject mProject) {
	}

}
