package philipp.schreitmueller.appeditor.apkediting;

/**
 * Enum for the different modes which are available for the editor
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public enum EditorMode {
	IMAGE, XML, SMALI, NOTAVAILABLE
}
