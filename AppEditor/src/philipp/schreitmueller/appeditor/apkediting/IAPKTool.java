package philipp.schreitmueller.appeditor.apkediting;

import philipp.schreitmueller.appeditor.utilities.APKProject;

/**
 * Defines the operations a apktool provider must implement
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public interface IAPKTool {

	public void decompile(APKProject project);

	public void compile(APKProject project);
}
