package philipp.schreitmueller.appeditor.apkediting;

/**
 * Represents a change in an EditText
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller adapted from
 *         https://code.google.com/r/credentiality-android-scripting/
 * 
 */
public class EditItem {
	private final int index;
	private final CharSequence before;
	private final CharSequence after;

	/**
	 * Constructs EditItem of a modification that was applied at position start
	 * and replaced CharSequence before with CharSequence after.
	 */
	public EditItem(int start, CharSequence mBefore, CharSequence mAfter) {
		index = start;
		before = mBefore;
		after = mAfter;
	}

	public int getIndex() {
		return index;
	}

	public CharSequence getBefore() {
		return before;
	}

	public CharSequence getAfter() {
		return after;
	}
}
