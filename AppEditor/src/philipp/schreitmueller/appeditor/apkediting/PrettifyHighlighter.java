package philipp.schreitmueller.appeditor.apkediting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prettify.PrettifyParser;
import syntaxhighlight.*;

/**
 * Helps interacting with the JavaPrettify jar-File
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class PrettifyHighlighter {
	private static final Map<String, String> COLORS = buildColorsMap();
	private static final String FONT_PATTERN = "<font color=\"%s\"><![CDATA[%s]]></font>";
	private final Parser parser = new PrettifyParser();

	/**
	 * Performs syntaxhighlighting with JavaPrettify
	 * 
	 * @param fileExtension
	 *            File ending to determine which programming language is used
	 * @param sourceCode
	 *            Code which should be highlighted
	 * @return Highlighted string
	 */
	public String highlight(String fileExtension, String sourceCode) {
		StringBuilder highlighted = new StringBuilder();
		List<ParseResult> results = parser.parse(fileExtension, sourceCode);
		for (ParseResult result : results) {
			String type = result.getStyleKeys().get(0);
			String content = sourceCode.substring(result.getOffset(),
					result.getOffset() + result.getLength());
			if (!content.contains("\n"))
				highlighted.append(String.format(FONT_PATTERN, getColor(type),
						content));
			else
				highlighted.append(content);
		}
		return highlighted.toString();
	}

	/**
	 * Gets the color for the given type
	 * 
	 * @param type
	 *            Type of the code snippet
	 * @return Corresponding color
	 */
	private String getColor(String type) {
		return COLORS.containsKey(type) ? COLORS.get(type) : COLORS.get("pln");
	}

	/**
	 * Creates a map in which colors are assigned to code types
	 * 
	 * @return The code color mapping
	 */
	private static Map<String, String> buildColorsMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("typ", "DarkGray");
		map.put("kwd", "blue");
		map.put("lit", "green");
		map.put("com", "green");
		map.put("typ", "green");
		map.put("dec", "green");
		map.put("src", "green");
		map.put("atn", "blue");
		map.put("atv", "green");
		map.put("str", "Olive");
		map.put("pun", "black");
		map.put("pln", "purple");
		return map;
	}
}