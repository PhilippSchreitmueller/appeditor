package philipp.schreitmueller.appeditor.apkediting;
/**
 * Enum for Online/Offline apktool usage
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 *
 */
public enum APKAccessMode {
	ONLINE,OFFLINE;
}
