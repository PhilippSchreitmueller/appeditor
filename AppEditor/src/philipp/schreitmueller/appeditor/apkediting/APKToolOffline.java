package philipp.schreitmueller.appeditor.apkediting;

import java.io.File;
import java.util.HashMap;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;
import android.widget.Toast;
import brut.androlib.Androlib;
import brut.androlib.ApkDecoder;
import brut.androlib.res.util.ExtFile;
import brut.common.BrutException;
import philipp.schreitmueller.appeditor.utilities.APKProject;

/**
 * Android version of apktool
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class APKToolOffline implements IAPKTool {
	private Context context;

	public APKToolOffline(Context mC) {
		context = mC;

	}

	@Override
	public void decompile(APKProject project) {
		ApkDecoder d = new ApkDecoder();
		String apkPAth = "";
		try {
			apkPAth = context.getPackageManager().getPackageInfo(
					project.getPackageName(), 0).applicationInfo.sourceDir;
		} catch (NameNotFoundException e1) {
			e1.printStackTrace();
		}
		// Create the necessary directories
		File mBase = new File(context.getFilesDir(), project.getPackageName());
		mBase.mkdir();
		File mSub = new File(mBase, project.getProjectName());
		mSub.mkdir();
		File mAPK = new File(mSub, "apktool");
		mAPK.mkdir();

		d.setApkFile(new File(apkPAth));
		try {
			d.setOutDir(mAPK);
			d.setForceDelete(true);

			// Lookup references if resources should be decompiled
			if (!PreferenceManager.getDefaultSharedPreferences(context)
					.getBoolean("pref_basics_decRes", true))
				d.setDecodeResources(Short.parseShort("256"));
			d.setFrameworkDir(context.getFilesDir() + "/exec/");
			d.decode();
		} catch (Exception e) {
			Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_LONG)
					.show();
			e.printStackTrace();
		}

	}

	@Override
	public void compile(APKProject project) {
		Androlib instance = new Androlib();
		HashMap<String, Boolean> flags = new HashMap<String, Boolean>();
		String orgApkPath = "";
		try {
			orgApkPath = context.getPackageManager().getPackageInfo(
					project.getPackageName(), 0).applicationInfo.sourceDir;
		} catch (NameNotFoundException e1) {
			e1.printStackTrace();
		}

		// Setup standard settings
		flags.put("forceBuildAll", false);
		flags.put("debug", true);
		flags.put("verbose", false);
		flags.put("injectOriginal", false);
		flags.put("framework", false);
		flags.put("update", false);
		try {
			instance.setFrameworkFolder(context.getFilesDir().getAbsolutePath()
					+ "/exec");
			instance.build(new File(project.getPath() + "/apktool"), new File(
					project.getPath() + "/apk/" + project.getApkName()), flags,
					new ExtFile(orgApkPath), context.getFilesDir()
							.getAbsolutePath() + "/exec/aapt");
		} catch (BrutException e) {
			Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_LONG)
					.show();
			e.printStackTrace();
		}
	}

}
