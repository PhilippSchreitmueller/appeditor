package philipp.schreitmueller.appeditor.apkediting;

import android.content.Context;
import philipp.schreitmueller.appeditor.utilities.APKProject;

/**
 * Access Facade for apktool. Supports compiling and decompiling of an apk.
 * 
 * @version 1.0 - 15.02.2014
 * @author Philipp Schreitmüller
 * 
 */
public class APKToolFacade {
	private Context context;
	private APKToolOffline apkOffline;
	private APKToolOnline apkOnline;

	/**
	 * Creates the facade
	 * 
	 * @param mC
	 *            The context of the app
	 */
	public APKToolFacade(Context mC) {
		context = mC;
		apkOffline = new APKToolOffline(context);
		apkOnline = new APKToolOnline();
	}

	/**
	 * Decomile a apk-File using apktool
	 * 
	 * @param mode
	 *            Either online or offline
	 * @param project
	 *            Project which should be dsecompiled
	 */
	public void decompile(APKAccessMode mode, APKProject project) {
		switch (mode) {
		case ONLINE:
			apkOnline.decompile(project);
			break;
		case OFFLINE:
			apkOffline.decompile(project);
			break;
		}
	}

	/**
	 * Compile a apk-Project back into an .apk-File
	 * 
	 * @param mode
	 *            Either online or offline
	 * @param project
	 *            Project which should be compiled
	 */
	public void compile(APKAccessMode mode, APKProject project) {
		switch (mode) {
		case ONLINE:
			apkOnline.compile(project);
			break;
		case OFFLINE:
			apkOffline.compile(project);
			break;
		}
	}

}
